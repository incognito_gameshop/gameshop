<%@ page import="java.io.*"%>
<%@ page import="gameshop.*"%>
<%@ page import="java.util.*"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Manage this Entry</title>
</head>
<body>
<%
String gID=request.getParameter("Id");
List<Games> gameList=new ArrayList<Games>();
GamesDAO gamesDAO=new GamesDAO();
gameList=gamesDAO.getGamesDetails(gID); 
Iterator<Games> itr = gameList.iterator();
%>
<table border="1" cellpadding="5">
<tr>
<th>ItemId</th>
<th>Price</th>
<th>Condition</th>
<th>Availability</th>
</tr>
<%
while(itr.hasNext())
{
	Games games=(Games)itr.next();
%>
<tr>
<td><a href=<%= "\"manageGame.jsp?itemId=" +games.getItemId() +"&gId="+games.getGameId()+ "\"" %> ><%=games.getItemId() %></a></td>
<td><%=games.getPrice() %></td>
<td><%=games.getCat() %></td>
<td><%=games.getAvailability() %></td>
</tr>
<%
}
%>
</table>
</body>
</html>