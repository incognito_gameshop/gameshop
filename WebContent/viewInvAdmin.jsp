<%@ page import="java.io.*"%>
<%@ page import="gameshop.*"%>
<%@ page import="java.util.*"%>
<%@ page import="gameshop.Inventory" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Results/Manage</title>
</head>
<body>
<%
int choice=Integer.parseInt(request.getParameter("property"));
String key=request.getParameter("prop");
List<Inventory> inv=new ArrayList<Inventory>();
InventoryDAO invDAO=new InventoryDAO();
inv=invDAO.getInventory(choice, key); 
Iterator<Inventory> itr = inv.iterator();
int i=1;
%>
<table border="1" cellpadding="5">
<tr>
<th>GameId</th>
<th>Title</th>
<th>Stock</th>
<th>Price</th>
</tr>
<% 
while(itr.hasNext())
{
	   Inventory inventory=(Inventory)itr.next();
%>
		<tr>
         <td><a href=<%= "\"manage.jsp?Id=" + inventory.getgameId() + "\"" %> ><%= inventory.getgameId() %></a></td>
         <td><%= inventory.getGName() %></td>
         <td><%= inventory.getStock() %></td>
         <td><%= inventory.getPrice() %></td>
     </tr>
<% } %>
</table>
</body>
</html>