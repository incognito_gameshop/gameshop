<%@ page import="java.io.*"%>
<%@ page import="gameshop.*"%>
<%@ page import="java.util.*"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Build Your Team</title>
</head>
<body>
<%
ArrayList<String> itemIds=new ArrayList<String>();
String netId=session.getAttribute("userid").toString();
ArrayList<Sales> sal=new ArrayList<Sales>();
SalesDAO salesDAO=new SalesDAO();
sal=salesDAO.getSaleDetails();
Iterator<Sales> itr = sal.iterator();
%>
   <table >
   <% 
   while(itr.hasNext())
   {
	   Sales sale=(Sales)itr.next();
	   if((sale.getNetID()).equalsIgnoreCase(netId))
	   {
		   itemIds.add(sale.getItemId());
	   }
   }
   for(String itemIdIt:itemIds)
   {
	 String gameId=itemIdIt.substring(0, 7);
	 for(Sales s:sal)
	 {
		if(s.getItemId().contains(gameId)&&!(s.getNetID().equalsIgnoreCase(netId))) 
  %>
 		<tr>
 
            <td><%= s.getNetID() %></td>
            
        </tr>
  <% }} %>
</table>
</body>
</html>