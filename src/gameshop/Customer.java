package gameshop;
import java.util.Date;
public class Customer 
{
	String nam;
	String netId;
	String address;
	String phone;
	String eMail;
	public Customer(String nam,String netId,String address,String phone, String eMail)
	{
		this.nam=nam;
		this.netId=netId;
		this.address=address;
		this.phone=phone;
		this.eMail=eMail;
	}
	public String getNam() {
		return nam;
	}
	public void setNam(String nam) {
		this.nam = nam;
	}
	public String getNetId() {
		return netId;
	}
	public void setNetId(String netId) {
		this.netId = netId;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String geteMail() {
		return eMail;
	}
	public void seteMail(String eMail) {
		this.eMail = eMail;
	}
}
