package gameshop;
import java.util.Date;
public class Games 
{
	String genre;
	String itemId;
	String gameId;
	String platform;
	String price;
	String cat;
	String availability;
	public Games(String genre,String itemId,String gameId,String platform,String price,String cat,String availability)
	{
		this.genre=genre;
		this.itemId=itemId;
		this.gameId=gameId;
		this.platform=platform;
		this.price=price;
		this.cat=cat;
		this.availability=availability;
	}
	public void setGenre(String genre)
	{
		this.genre=genre;
	}
	public void setItemId(String itemId)
	{
		this.itemId=itemId;
	}
	public void setGameId(String gameId)
	{
		this.gameId=gameId;
	}
	public void setPlatform(String platform)
	{
		this.platform=platform;
	}
	public void setPrice(String price)
	{
		this.price=price;
	}
	public void setCat(String cat)
	{
		this.cat=cat;
	}
	public void setAvailabilty(String availability)
	{
		this.availability=availability;
	}
	public String getGenre()
	{
		return this.genre;
	}
	public String getItemId()
	{
		return this.itemId;
	}
	public String getGameId()
	{
		return this.gameId;
	}
	public String getPlatform()
	{
		return this.platform;
	}
	public String getPrice()
	{
		return this.price;
	}
	public String getCat()
	{
		return this.cat;
	}
	public String getAvailability()
	{
		return this.availability;
	}
}
