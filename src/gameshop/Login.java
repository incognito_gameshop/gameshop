package gameshop;

public class Login 
{
	String UName;
	String PSWD;
	String Flag;
	public Login(String UName,String PSWD,String Flag)
	{
		this.UName=UName;
		this.PSWD=PSWD;
		this.Flag=Flag;
	}
	public String getUName() {
		return UName;
	}
	public void setUName(String uName) {
		UName = uName;
	}
	public String getPSWD() {
		return PSWD;
	}
	public void setPSWD(String pSWD) {
		PSWD = pSWD;
	}
	public String getFlag() {
		return Flag;
	}
	public void setFlag(String flag) {
		Flag = flag;
	}
	
}
