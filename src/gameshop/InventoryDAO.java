package gameshop;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import java.util.Date;

import com.mysql.jdbc.Statement;
public class InventoryDAO 
{
	DateFormat df=new SimpleDateFormat("yyyy-MM-dd");;
	String gameId;
	String gName;
	//Date rDate;
	String price;
	int stock;
	Inventory inventory;
	public ArrayList<Inventory> getInventory(int choice,String key)
	{
		ResultSet rs = null;
		ArrayList<Inventory> inv=new ArrayList<Inventory>();
		try
		{
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			Connection mycon = null;
			mycon=	DriverManager.getConnection("jdbc:mysql://localhost:3306/gameshop?useSSL=false","root","");
			Statement st= (Statement) mycon.createStatement();
			if(choice==1)
			{
				rs=st.executeQuery("select distinct i.GameId,i.GName,i.Price,count(g.ItemId) from inventory i,games g where g.GameId=i.GameId and g.genre like '%"+key+"%' group by(i.GameId)");
			}
			else if(choice==2)
			{
				rs=st.executeQuery("select distinct i.GameId,i.GName,i.Price,count(g.ItemId) from inventory i,games g where g.GameId=i.GameId and i.GName like '%"+key+"%' group by(i.GameId)");
			}
			else if(choice==3)
			{
				rs=st.executeQuery("select distinct i.GameId,i.GName,i.Price,count(g.ItemId) from inventory i,games g where g.GameId=i.GameId and g.platform like '%"+key+"%'group by(i.GameId) ");
			}
			while(rs.next())
			{
				gameId=rs.getString(1);
				gName=rs.getString(2);
				price=rs.getString(3);
				stock=rs.getInt(4);
				inventory=new Inventory(gameId,gName,price,stock);
				inv.add(inventory);
			}
			mycon.close();
			rs.close();
		}
		catch(Exception e){}
		return inv;
	}
	public void addInventory(String GName,String price,int stock,String platform,String category,String genre)
	{
		String gameId="";
		String itemId="";
		String count="";
		int c=0;
		ResultSet rs = null;
		try
		{
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			Connection mycon = null;
			mycon=	DriverManager.getConnection("jdbc:mysql://localhost:3306/gameshop?useSSL=false","root","");
			Statement st= (Statement) mycon.createStatement();
				gameId=gameId+platform.toUpperCase()+"0";
				itemId=itemId+platform.toUpperCase()+"0";
				//Compare Game names to get the sequence number//
				if(GName.equalsIgnoreCase("Titanfall"))
				{
					gameId+="1234";
					itemId+="1234";
				}
				if(GName.equalsIgnoreCase("Doom"))
				{
					gameId+="1222";
					itemId+="1222";
				}
				if(platform.equalsIgnoreCase("XB"))
					platform="XBox1";
				else
					if(platform.equalsIgnoreCase("PC"))
						platform="PC";
					else
						if(platform.equalsIgnoreCase("PS"))
							platform="PlayStation";
			rs=st.executeQuery("select max(ItemId) from games where GameId='"+gameId+"'group by GameId");
			while(rs.next())
			{
				if(!(rs.getString(1).isEmpty()))
				c=Integer.parseInt((rs.getString(1)).substring(7));
			}
			st= (Statement) mycon.createStatement();
			for(int i=1;i<=stock;i++)
			{
				c++;
				st.executeUpdate("insert into games values('"+genre+"','"+gameId+"','"+(itemId+String.valueOf(c))+"','"+platform+"','"+price+"','"+category+"','T')");
			}
			st.close();
			st= (Statement) mycon.createStatement();
			System.out.println("insert into inventory values('"+gameId+"','"+GName+"','"+price+"')");
			st.executeUpdate("insert into inventory values('"+gameId+"','"+GName+"','"+price+"')");
			mycon.close();
			rs.close();
		}
		catch(Exception e){}
	}
}
