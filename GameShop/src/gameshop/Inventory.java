package gameshop;
import java.util.Date;
public class Inventory 
{
	String gameId;
	String gName;
	String price;
	int stock;
	public Inventory(String gameId,String gName, String price,int stock)
	{
		this.gameId=gameId;
		this.gName=gName;
		this.price=price;
		this.stock=stock;
	}
	public void setgameId(String gameId)
	{
		this.gameId=gameId;
	}
	public void setGName(String gName)
	{
		this.gName=gName;
	}
	public void setPrice(String price)
	{
		this.price=price;
	}
	public String getgameId()
	{
		return this.gameId;	
	}
	public String getGName()
	{
		return this.gName;
	}
	public String getPrice()
	{
		return this.price;
	}
	public int getStock()
	{
		return this.stock;
	}
}
