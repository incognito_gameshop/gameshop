package gameshop; 
public class Contact{
	
	String name;
	String email;
	String netId;
	String message;
	int phone;
	public Contact(String name,String email,String netId,String message,int phone){
		this.name=name;
		this.email=email;
		this.netId=netId;
		this.message=message;
		this.phone=phone;
		
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNetId() {
		return netId;
	}
	public void setNetId(String netId) {
		this.netId = netId;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getPhone() {
		return phone;
	}
	public void setPhone(int phone) {
		this.phone = phone;
	}
	
	
}