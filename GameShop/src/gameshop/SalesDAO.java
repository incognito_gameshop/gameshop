package gameshop;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.ArrayList;

import java.util.Date;

import com.mysql.jdbc.Statement;
public class SalesDAO
{
	String saleAmt;
	String saleId;
	String netId;
	String itemId;
	Date saleDate;
	Sales sale;
	public ArrayList<Sales> getSaleDetails(int choice,String key)
	{
		ArrayList<Sales> sal=new ArrayList<Sales>();
		ResultSet rs=null;
		
		try
		{
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			Connection mycon = null;
			mycon=	DriverManager.getConnection("jdbc:mysql://localhost:3306/gameshop?useSSL=false","root","root");
			Statement st= (Statement) mycon.createStatement(); 
			//ResultSet rs=st.executeQuery("select * from sales");
			if(choice==1)
			{
				rs=st.executeQuery("select distinct * from sales where SaleId like '%"+key+"%'");
			}
			if(choice==2)
			{
				rs=st.executeQuery("select distinct * from sales where SaleAmt like '%"+key+"%'");
			}
			while(rs.next())
			{
				saleId=rs.getString(1);
				itemId=rs.getString(2);
				netId=rs.getString(3);
				saleAmt=rs.getString(4);
				saleDate=rs.getDate(5);
				sale=new Sales(saleDate,saleId,itemId,netId,saleAmt);
				sal.add(sale);
			}
			mycon.close();
			rs.close();
		}
		catch(Exception e){}
		return sal;
	}
}