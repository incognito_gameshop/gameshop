package gameshop; 
import java.util.Date;
public class Sales 
{
Date saleDate;
String saleId;
String itemId;
String netId;
String saleAmt;
public Sales(Date saleDate, String saleId, String itemId, String netId, String saleAmt)
{
	this.saleId=saleId;
	this.saleDate=saleDate;
	this.itemId=itemId;
	this.netId=netId;
	this.saleAmt=saleAmt;
}
public void setSaleDate(Date saleDate)
{
	this.saleDate=saleDate;
}
public void setSaleId(String saleId)
{
	this.saleId=saleId;
}
public void setSaleAmt(String saleAmt)
{
	this.saleAmt=saleAmt;
}
public void setItemId(String itemId)
{
	this.itemId=itemId;
}
public void setNetId(String netId)
{
	this.netId=netId;
}
public Date getSaleDate()
{
	return this.saleDate;
}
public String getSaleId()
{
	return this.saleId;
}
public String getNetID()
{
	return this.netId;
}
public String getItemId()
{
	return this.itemId;
}
public String getSaleAmt()
{
	return this.saleAmt;
}
}