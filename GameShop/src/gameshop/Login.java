package gameshop;

public class Login {
	String un;
	String pw;
	String auth;
	String flag;
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public String getAuth() {
		return auth;
	}
	public void setAuth(String auth) {
		this.auth = auth;
	}
	public Login()
	{}
	public Login(String un, String pw, String flag)
	{
		this.un = un;
		this.pw = pw;
		this.flag = flag;
	}
	public String getUn() {
		return un;
	}
	public void setUn(String un) {
		this.un = un;
	}
	public String getPw() {
		return pw;
	}
	public void setPw(String pw) {
		this.pw = pw;
	}
}
