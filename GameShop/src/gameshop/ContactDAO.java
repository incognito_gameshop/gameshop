package gameshop;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.mysql.jdbc.Statement;

public class ContactDAO{
	
	String name;
	String email;
	String netId;
	String message;
	int phone;
	Contact contact;
	
	public ArrayList<Contact> getcontactdetails(int choice,String key){
		
		ArrayList<Contact> cont=new ArrayList<Contact>();
		ResultSet rs=null;
		
		
		try
		{
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			Connection mycon = null;
			mycon=	DriverManager.getConnection("jdbc:mysql://localhost:3306/gameshop?useSSL=false","root","root");
			Statement st= (Statement) mycon.createStatement(); 
			if(choice==1)
			{
				rs =st.executeQuery("select distinct * from contact where NetId like '%"+key+"%'");
			}
			if(choice==2)
			{
				rs=st.executeQuery("select distinct * from contact where Email like '%"+key+"%'");
			}
			
			while(rs.next())
			{
				name=rs.getString(1);
				email=rs.getString(2);
				phone=rs.getInt(3);
				netId=rs.getString(4);
				message=rs.getString(5);
				
				contact=new Contact(name,email,netId,message,phone);
				cont.add(contact);
				
				
				
				
				
			}
			mycon.close();
			rs.close();
		
		}
		catch(Exception e){}
		return cont;
	}
}