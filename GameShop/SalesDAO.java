package gameshop;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SalesDAO{

public void find() throws SQLException {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        List<Sales> products = new ArrayList<Sales>();

        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/gameshop?useSSL=false","root","root");
            statement = connection.prepareStatement("select * from sales");
            
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Sales product = new Sales();
                product.setsalesid(resultSet.getString("SaleId"));
                product.setnetid(resultSet.getString("NetId"));
                product.setitemid(resultSet.getString("ItemId"));
               // product.setPrice(resultSet.getBigDecimal("price"));
                products.add(product);
            }
        } finally {
            if (resultSet != null) try { resultSet.close(); } catch (SQLException logOrIgnore) {}
            if (statement != null) try { statement.close(); } catch (SQLException logOrIgnore) {}
            if (connection != null) try { connection.close(); } catch (SQLException logOrIgnore) {}
        }

       // return products;
        
        for(int i=0;i<products.size();i++){
        	System.out.println(products.get(i).getnetid());
        }
    }

}