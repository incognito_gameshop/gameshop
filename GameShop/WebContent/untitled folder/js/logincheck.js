
function loginusingfb() {
FB.api('/me?fields=id,name,email',function(response){
    	  console.log(JSON.stringify(response));
    	  
    	  var json = {};
			json["emailId"] = response.email;
			json["name"] = response.name;
			json["fb"]=1;
			var jsonStr = JSON.stringify(json);
			var postURL = "https://localhost:8443/utdeats/login";
			var req = $.post(postURL, json)
			.done(function(data) {
					var userId = data["userId"];
					var lastLoginTime = data["lastLoginTime"];
					var failedLoginCount = data["failedLoginCount"];
					if(userId != null) {
						sessionStorage.userId = userId;
						sessionStorage.lastLoginTime=lastLoginTime;
						sessionStorage.failedLoginCount=failedLoginCount;

						window.location.replace("https://localhost:8443/utdeats/index.html");
					} else {
						$("#invalidLogin").text("Invalid username/password. Please try again!");
					}
			})

				.fail(function(data) {
					$("#invalidLogin").text("Server error. Please try again!");
		    });
		});
}

function login(emailId, password){
	
	var json = {};
	json["emailId"] = emailId;
	json["password"] = password;
	var jsonStr = JSON.stringify(json);
	var postURL = "https://localhost:8443/utdeats/login";
	var req = $.post(postURL, json)
		.done(function(data) {
					var userId = data["userId"];
					var lastLoginTime = data["lastLoginTime"];
					var failedLoginCount = data["failedLoginCount"];
					if(userId != null) {
						sessionStorage.userId = userId;
						sessionStorage.lastLoginTime=lastLoginTime;
						sessionStorage.failedLoginCount=failedLoginCount;

						window.location.replace("https://localhost:8443/utdeats/index.html");
					} else {
						$("#invalidLogin").text("Invalid username/password. Please try again!");
					}
			})

		.fail(function(data) {
			$("#invalidLogin").text("Server error. Please try again!");
    });
}
     
function clicklgb(){
$("#logoutb").click(function(){
	sessionStorage.userId = 0;
	sessionStorage.lastLoginTime=0;
	sessionStorage.failedLoginCount=0;
	window.location = "https://localhost:8443/utdeats/login.html";
	
});
}


function checklogin(){
	//sessionStorage.userId=5;
	if(sessionStorage.userId==0)
		
		window.location = "https://localhost:8443/utdeats/login.html";
}
