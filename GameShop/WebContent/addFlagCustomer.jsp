<%@ page import="java.io.*"%>
<%@ page import="gameshop.*"%>
<%@ page import="java.util.*"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add New Customer</title>
</head>
<body>
<form action="addCustomer.jsp" method="post">
<table border="1" cellpadding="5">
<tr><td>Name</td><td><input type="text" name="custName"/></td></tr>
<tr><td>Email</td><td><input type="text" name="custEmail"/></td></tr>
<tr><td>Phone</td><td><input type="text" name="custPhone"/></td></tr>
<tr><td>Address</td><td><input type="text" name="custAddress"/></td></tr>
<tr><td>NetId</td><td><input type="text" name="netId"></td></tr>
<tr><td>Password</td><td><input type="text" name="PSWD"></td></tr>
</table>
<input type="submit" value="Add Customer">
</form>
<hr></hr>
<form action="flagCustomer.jsp" method="post">
<table border="1" cellpadding="5">
<tr>
<td>NetId</td><td><input type="text" name="flagNetId"/></td>
<td><select name="choice">
    <option value="1">Flag</option>
    <option value="2">UnFlag</option>
  </select></td>
</tr>
</table>
<input type="submit" value="Flag Customer">
</form>
</body>
</html>