
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>View Inventory</title>
</head>
<body>

Search By : 
<form action="viewInvAdmin.jsp" method="post">
  <select name="property">
    <option value="1">Genre</option>
    <option value="2">Title</option>
    <option value="3">Platform</option>
  </select>
  <input type="text" name="prop"/>
  <input type="submit" value="Submit">
</form>
<hr></hr>
<form action="addInventory.jsp" method="post">
<table border="1" cellpadding="5">
<tr><td>Game Name</td><td><input type="text" name="GName"/></td></tr>
<tr><td>Price</td><td><input type="text" name="price"/></td></tr>
<tr><td>Stock</td><td><input type="text" name="stock"/></td></tr>
<tr><td>Platform</td><td><select name="platform">
    <option value="XB">XBox</option>
    <option value="PC">PC</option>
    <option value="PS">PlayStation</option>
  </select></td></tr>
  <tr><td>Category</td><td><select name="cat">
    <option value="N">New</option>
    <option value="U">Used</option>
  </select></td></tr>
  <tr><td>Genre</td><td><select name="genre">
    <option value="Action">Action</option>
    <option value="RPG">RPG</option>
    <option value="RTS">RTS</option>
  </select></td></tr>
</table>
<input type="submit" value="Add">
</form>
</body>
</html>