<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Profile</title>

	
	<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">

    <!-- Custom Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css" type="text/css">

    <!-- Plugin CSS -->
    <link rel="stylesheet" href="css/magnific-popup.css" type="text/css">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/creative.css" type="text/css">

</head>

	

 
    
   
    <body style="background-color: #121212" >
	<%@ page import="java.sql.*"%>
	<%@ page import="javax.sql.*"%>
	<%@ page import="gameshop.*"%>
	<%@ page import="java.io.*"%>
	<%@ page import="java.util.*"%>
    <%
    
    String a=session.getAttribute("userid").toString();
    
    Customer cust = new Customer(a);
    ProfileDAO profileDao = new ProfileDAO();
    profileDao.getCustProfile(cust);
     %>
     <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand page-scroll" href="index.jsp">GameShop</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="page-scroll" href="index.html#about">About</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="index.html#services">Services</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="index.html#portfolio">Portfolio</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="index.html#contact">Contact</a>
                    </li>
					<li>
                        <a class="page-scroll" href="userprofile.html">Profile</a>
                    </li>
					<li>
                        <a class="page-scroll" href="login.html">Login</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    
    <br/>
	<link rel="stylesheet" href="css/form-register.css">
     <div class="main-content">

        <!-- You only need this form and the form-register.css -->

        <form class="form-register" method="post" action="updateProfile.jsp">

            <div class="form-register-with-email">

                <div class="form-white-background">

                    <div class="form-title-row">
                        <h1>Edit your profile</h1>
                    </div>

                    <div class="form-row">
                        <label>
                            <span>Name</span>
                            <input style="color:black" id="name" type="text" name="name" value="<% out.print(cust.getName());%>">
                        </label>
                    </div>
					
					<div class="form-row">
                        <label>
                            <span>Phone Number</span>
                            <input style="color:black" id="phno" type="text" name="phone" value="<% out.print(cust.getPhone());%>">
                        </label>
                    </div>

                    <div class="form-row">
                        <label>
                            <span>Email</span>
                            <input  style="color:black" id="email" type="text" name="email" value="<% out.print(cust.getEmail());%>">
                        </label>
                    </div>

                    <div class="form-row">
                        <label>
                            <span>Address</span>
                            <input style="color:black" id="pass" type="text" name="address" value="<% out.print(cust.getAddress());%>">
                        </label>
                    </div>
		
                    

                    <div class="form-row">
                        <button id="submit" type="submit">Update</button>
                    </div>

                </div>


            </div>

            

        </form>

    </div>

	  <script>
	  
					$(document).ready(function() {
						 $.getJSON('https://localhost:8443/utdeats/createUser',function(ordertable){
							 var name= ordertable.name;
							 var phno= ordertable.phoneNo;
							 var email= ordertable.emailId;
							 var pass= ordertable.password;
						
							 $('#name').val(name);
							 $('#phno').val(phno);
							 $('#email').val(email);
							 $('#pass').val(pass);
							 localStorage.setItem('obj',JSON.stringify(ordertable));
							 
						 });
						 
						 $('#submit').click(function() {
							var temp = JSON.parse(localStorage.getItem('obj'));
							var nm=$('#name').val();
							temp.name=nm;
							var phn=$('#phno').val();
							temp.phNo=phn;
							var em=$('#email').val();
							temp.emailId=em;
							var ps=$('#pass').val();
							temp.password=ps;
							temp.updated=1;	
							$.post("https://localhost:8443/utdeats/createUser", temp);
							
						 });
						 $("#logoutb").click(function(){
								clicklgb();		
						});
						 
						 var failedc= sessionStorage.getItem('failedLoginCount');
				
						 var time = sessionStorage.getItem('lastLoginTime');
						 var time1=parseInt(time,10);
						 var date = new Date(time1);
					
						
						 jQuery("label[for='myalue']").html(date);
						 jQuery("label[for='failedcount']").html(failedc);
						 
					});
		</script>
		

</body>

</html>