<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Update Profile</title>
</head>
<body>
	<%@ page import="java.sql.*"%>
	<%@ page import="javax.sql.*"%>
	<%@ page import="gameshop.*"%>
	<%@ page import="java.io.*"%>
	<%@ page import="java.util.*"%>
<%
String name  = request.getParameter("name").toString();
String phone = request.getParameter("phone").toString();
String email  = request.getParameter("email").toString();
String address = request.getParameter("address").toString();
String a=session.getAttribute("userid").toString();
Customer cust = new Customer(a);
cust.setName(name);
cust.setPhone(phone);
cust.setEmail(email);
cust.setAddress(address);
ProfileDAO profileDao = new ProfileDAO();
profileDao.editCustProfile(cust);
%>
<script>
window.alert("Customer Edit Successfully");
window.location.href = "userprofile.jsp";
</script>

</body>
</html>